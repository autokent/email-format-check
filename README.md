# email-format-check
> **E-Mail address format validation module.**
> **Also check user and domain part max length**

![logo](https://assets.gitlab-static.net/uploads/-/system/project/avatar/4802389/email-format-check.png)

[![version](https://img.shields.io/npm/v/email-format-check.svg)](https://www.npmjs.org/package/email-format-check)
[![downloads](https://img.shields.io/npm/dt/email-format-check.svg)](https://www.npmjs.org/package/email-format-check)
[![node](https://img.shields.io/node/v/email-format-check.svg)](https://nodejs.org/)
[![status](https://gitlab.com/autokent/email-format-check/badges/master/pipeline.svg)](https://gitlab.com/autokent/email-format-check/pipelines)

## Installation
`npm install email-format-check`

Syntax email addresses verification based on [RFC5321](https://tools.ietf.org/html/rfc5321) and 
[RFC5322](https://tools.ietf.org/html/rfc5322).

## Usage
```js
const isemail = require('email-format-check');
console.log(isemail("user@domain.com"));//true
console.log(isemail("user.domain.com"));//false
```


## Test
`mocha` or `npm test`

> check test folder and QUICKSTART.js for extra usage.