const assert = require('assert');
const efc = require('../');

describe('valid email test', function() {
    it('should return true', function() {
        assert.equal(efc("user@domain.com"),true);
    });
});


describe('invalid email check', function() {
    it('should return false', function() {
        assert.equal(efc("user.domain.com"),false);
    });
});

